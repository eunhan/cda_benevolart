/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@SequenceGenerator(name="authentification_id_seq", initialValue = 1, allocationSize=1)
public class Authentification  {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="authentification_id_seq")
	private Long id;
	
	
	private String email;
	
	
	private String motPasse;
	
	@OneToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn()
	private Utilisateur utilisateur;	
	

}
