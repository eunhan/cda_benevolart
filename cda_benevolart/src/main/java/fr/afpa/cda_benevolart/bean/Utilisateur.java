/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@SequenceGenerator(name = "utilisateur_id_seq", initialValue = 1, allocationSize = 1)

public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_id_seq")
	private Long id;

	private String pseudo;

	private String telephone;

	@Column
	private int avertissement;

	@Column
	private boolean active;

//	@OneToOne(mappedBy="utilisateur")
//	@JoinColumn()
//	private Authentification authentification;

	@ManyToOne
	@JoinColumn()
	private Roles roles;

//	@OneToMany(mappedBy = "utilisateur")
//	private List<Message> ltMessage;

//	@OneToMany(mappedBy = "utilisateur")
//	@JsonBackReference (value="utilisateur_annonce")
//	private List<Annonce> ltAnnonceMettre;

//	@ManyToMany(mappedBy="ltBenevol")
//	private List<Annonce> ltAnnonceParticip;

}
