/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@SequenceGenerator(name = "annonce_id_seq", initialValue = 1, allocationSize = 1)
public class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "annonce_id_seq")
	private Long id;

	private boolean offre;

	private boolean demande;

	private String objet;

	@Column(columnDefinition="TEXT")
	private String contenu;

	private int nombreBesoin;

	private LocalDateTime dateTimePub;

	private LocalDateTime dateTimeDebut;

	private LocalDateTime dateTimeFin;

	
	private boolean visibilite;

//	@OneToMany(mappedBy = "annonce")
//	private List<Message> ltMessage;

//	@OneToMany(mappedBy = "annonce")
//	private List<Url> ltUrl;

	@ManyToOne
	@JoinColumn()
	private SousCategorie sousCategorie;

	@ManyToOne(cascade= {CascadeType.PERSIST})
	@JoinColumn()
	private Adresse adresse;

	@ManyToOne
	@JoinColumn()
	private Utilisateur utilisateur;

	@ManyToMany
	private List<Utilisateur> ltBenevol;
}
