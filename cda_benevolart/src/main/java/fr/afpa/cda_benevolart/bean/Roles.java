/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@SequenceGenerator(name = "roles_id_seq", initialValue = 1, allocationSize = 1)
public class Roles {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_id_seq")
	private Long id;

	private String poste;

//	@OneToMany(mappedBy = "roles", cascade= {CascadeType.PERSIST})
//	@JsonBackReference(value="roles_utilisateur")
//	private List<Utilisateur> ltUtilisateur;

}
