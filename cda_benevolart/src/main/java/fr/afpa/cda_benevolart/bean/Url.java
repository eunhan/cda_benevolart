/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@SequenceGenerator(name="url_id_seq", initialValue = 1, allocationSize=1)
public class Url {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="url_id_seq")
	private Long id;
	
	
	private String url;
	
	@ManyToOne //(cascade= {CascadeType.PERSIST})
	@JoinColumn()
	private Annonce annonce;
	

}
