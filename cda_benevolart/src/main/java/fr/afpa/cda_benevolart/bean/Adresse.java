/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */



@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@SequenceGenerator(name = "adresse_id_seq", initialValue = 1, allocationSize = 1)
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adresse_id_seq")
	private Long id;

	private int numVoie;

	private String typeVoie;

	private String libelleRue;

	private String codePostal;

	private String ville;

	private String pays;

	private String adresseSupl;

//	@OneToMany(mappedBy = "adresse", cascade= {CascadeType.PERSIST})
//	@JsonBackReference(value="adresse_annonce")
//	private List<Annonce> ltAnnonce;

}
