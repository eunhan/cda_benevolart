/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@SequenceGenerator(name="categorie_id_seq", initialValue = 1, allocationSize=1)
public class Categorie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="categorie_id_seq")
	private Long id;
	
	
	private String libelle;
	
	@OneToMany(mappedBy="categorie")
	private List<SousCategorie> ltSousCategorie;
	

}
