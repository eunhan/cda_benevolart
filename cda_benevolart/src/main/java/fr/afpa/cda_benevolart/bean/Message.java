/**
 * 
 */
package fr.afpa.cda_benevolart.bean;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Eun Kyung HAN
 *
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@SequenceGenerator(name="message_id_seq", initialValue = 1, allocationSize=1)
public class Message {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator ="message_id_seq")
	private Long id;
	
	
	private Long idCorrespondant;
	
	private boolean envoi;
	
	private boolean recu;
	
	private String objet;
	
	@Column(columnDefinition="TEXT")
	private String contenu;
	
	
	private LocalDateTime dateTimeMsg;
	
	private boolean archive;
	
	private boolean abus;
	
	private boolean commentaire;
	
	private boolean msg;
	
	@ManyToOne
	@JoinColumn()
	private Utilisateur utilisateur;
	
	@ManyToOne
	@JoinColumn()
	private Annonce annonce;
	
	
}
