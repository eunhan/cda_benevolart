/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Adresse;
/**
 * @author Eun Kyung HAN
 *
 */
@Repository
public interface IDaoAdresse extends JpaRepository<Adresse, Long> {

}
