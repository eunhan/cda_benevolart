/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Authentification;

@Repository
public interface IDaoAuthentification extends JpaRepository<Authentification, Long> {
	
	Authentification findByEmailAndMotPasse(String email, String motPasse);
	
	Authentification findByEmail(String email);
	
	@Query(value="select * from authentification where utilisateur_id= :id", nativeQuery=true)
	Authentification findByUtilisateurId(@Param("id") Long id);
	
//	@Query(value="select a.id, a.email, a.mot_passe, a.utilisateur_id from authentification a, utilisateur u where a.email=:email and a.mot_passe=:motPasse and a.utilisateur_id=u.id and u.active=true", nativeQuery=true)
//	Authentification findByEmailAndMotPasseAndActive(@Param("email")String email, @Param("motPasse")String motPasse);
	
	
}
