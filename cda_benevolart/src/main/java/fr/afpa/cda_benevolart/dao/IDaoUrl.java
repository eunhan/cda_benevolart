/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Url;

/**
 * @author Eun Kyung
 *
 */
@Repository
public interface IDaoUrl extends JpaRepository<Url, Long>{

}
