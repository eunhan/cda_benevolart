/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Roles;

/**
 * @author Eun Kyung HAN
 *
 */
@Repository
public interface IDaoRoles extends JpaRepository<Roles, Long>{
	
	Roles findByPoste(String poste);

}
