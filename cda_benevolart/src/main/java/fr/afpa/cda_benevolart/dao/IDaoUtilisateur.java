/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Utilisateur;

/**
 * @author Eun Kyung HAN
 *
 */
@Repository
public interface IDaoUtilisateur extends JpaRepository<Utilisateur, Long> {

}
