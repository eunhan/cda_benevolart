/**
 * 
 */
package fr.afpa.cda_benevolart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.afpa.cda_benevolart.bean.Annonce;
/**
 * @author Eun Kyung HAN
 *
 */
@Repository
public interface IDaoAnnonce extends JpaRepository<Annonce, Long>{
	
	List<Annonce> findByVisibilite (boolean verify);
	
	
//	@Query(value= "select * from annonce a where a.sous_categorie_id=:categorie and (a.objet like '%'||:cle||'%' or a.contenu like '%'||:cle||'%' or a.contenu like :cle||'%' or a.contenu like '%'||:cle)", nativeQuery=true)   ok
//	List<Annonce> findByCritere(@Param("categorie") Long categorie, @Param("cle") String cle);

	
//	@Query(value= "select * from annonce a where a.sous_categorie_id=:categorie and (a.objet like '%'||:cle||'%' or a.contenu like '%'||:cle||'%' or a.contenu like :cle||'%' or a.contenu like '%'||:cle)", nativeQuery=true)   ok
//	List<Annonce> findByCritere(@Param("categorie") Long categorie, @Param("cle") String cle);
	
	@Query(value="select * from annonce a where a.utilisateur_id=:id", nativeQuery=true)
	List<Annonce> findByUtilisateurId (Long id);
	
	@Query(value="select * from annonce a where a.demande=true and a.sous_categorie_id=:categorie and (a.objet like '%'||:cle||'%' or a.contenu like '%'||:cle||'%' or a.contenu like :cle||'%' or a.contenu like '%'||:cle)", nativeQuery=true)
	List<Annonce> findByCritereDemande(@Param("categorie") Long categorie, @Param("cle") String cle);
	
	
	@Query(value="select * from annonce a where a.offre=true and a.sous_categorie_id=:categorie and (a.objet like '%'||:cle||'%' or a.contenu like '%'||:cle||'%' or a.contenu like :cle||'%' or a.contenu like '%'||:cle)", nativeQuery=true)
	List<Annonce> findByCritereOffre(@Param("categorie") Long categorie, @Param("cle") String cle);
	
	

}
