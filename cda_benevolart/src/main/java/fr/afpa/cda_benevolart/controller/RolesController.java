/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.cda_benevolart.bean.Roles;
import fr.afpa.cda_benevolart.service.IServiceRoles;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class RolesController {
	
	@Autowired
	IServiceRoles serviceRoles;
	
	@RequestMapping(value="/findRole{id}", method=RequestMethod.GET)
	public MappingJacksonValue findRole(@PathVariable Long id) {
		Roles role = serviceRoles.findRole(id);
		MappingJacksonValue roleRetour = new MappingJacksonValue(role);
		return roleRetour;
	}

}
