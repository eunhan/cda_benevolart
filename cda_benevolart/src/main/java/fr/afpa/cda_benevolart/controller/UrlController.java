/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.afpa.cda_benevolart.bean.Url;
import fr.afpa.cda_benevolart.service.IServiceUrl;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
public class UrlController {
	
	@Autowired
	IServiceUrl serviceUrl;
	
	/**
	 * Methode permettant d'ajouter une liste d'url
	 * 
	 * @param listeUrl : Une liste d'url de l'utilisateur
	 * @return	: ResponseEntity type Boolean
	 */
	@RequestMapping(value="/ajouter", method=RequestMethod.POST)
	public ResponseEntity<Boolean> ajouter(@RequestBody List<Url> listeUrl ) {
		
		List<Url> listeRetour = serviceUrl.ajouter(listeUrl);
		
		if(listeRetour == null) {
			return ResponseEntity.noContent().build();
		}
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("{id}") //?
				.buildAndExpand(listeRetour.get(0).getId()) //?
				.toUri();
		boolean verify = true;
		
		return ResponseEntity.created(location).body(verify);
	}
}
