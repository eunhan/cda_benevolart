/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.afpa.cda_benevolart.bean.Authentification;
import fr.afpa.cda_benevolart.bean.Roles;
import fr.afpa.cda_benevolart.bean.Utilisateur;
import fr.afpa.cda_benevolart.service.IServiceAuthentification;
import fr.afpa.cda_benevolart.service.IServiceRoles;
import fr.afpa.cda_benevolart.service.IServiceUtilisateur;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class AuthentificationController {

	@Autowired
	IServiceAuthentification serviceAuthen;
	
	@Autowired
	IServiceUtilisateur serviceUser;
	
	@Autowired
	IServiceRoles serviceRoles;

	/**
	 * Methode permettant d'aller chercher l'utilisateur
	 * 
	 * @param authentification : les donnees saisies par l'utilisateur
	 * @return : s'il ne trouve rien return ResponseEntity sans content sinon return
	 *         responseentity type utilisateur cree avec les informations
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Utilisateur> login(@RequestBody Authentification authentification) {
		
		Utilisateur resultat = serviceAuthen.login(authentification);		
		
		if (resultat == null) {
			return ResponseEntity.noContent().build();
		}
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(resultat.getId())
				.toUri();

		return ResponseEntity.created(location).body(resultat);
	}

	
	/*@RequestMapping(value = "/login2", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<Utilisateur> login2(@RequestBody Authentification authentification) {
		
		Utilisateur resultat = serviceAuthen.login(authentification);		
		
		if (resultat == null) {
			return ResponseEntity.noContent().build();
		}
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(resultat.getId())
				.toUri();

		return ResponseEntity.created(location).body(resultat);
	}*/
	

	/**
	 * Methode permettant d'inscrire un nouveau authentification
	 * 
	 * @param authentification : les informations saisies par l'utilisateur
	 * @return				  : boolean
	 */
	@RequestMapping(value = "/inscription", method = RequestMethod.POST)
	public ResponseEntity<Boolean> inscription(@RequestBody Authentification authentification) {
		
		if ( !serviceAuthen.verifierEmail(authentification.getEmail())) {
			return ResponseEntity.noContent().build();
		}
		authentification.getUtilisateur().setRoles(new Roles(2L,"user")); 
		Authentification retourAuth = serviceAuthen.inscription(authentification);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(retourAuth.getId())
				.toUri();
		boolean verify = true;

		return ResponseEntity.created(location).body(verify);
	}
	
	
	@RequestMapping(value = "/getAuthentification", method = RequestMethod.POST)
	public ResponseEntity<Authentification> getAuthentification(@RequestBody Long id) {
		
		Authentification authentification = serviceAuthen.getAuthentification(id);
		
		if ( authentification==null) {
			return ResponseEntity.noContent().build();
		}
		
		
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(authentification.getId())
				.toUri();
		
		return ResponseEntity.created(location).body(authentification);
	}
	
	
	@RequestMapping(value = "/modifier", method = RequestMethod.POST)
	public ResponseEntity<Utilisateur> modifier(@RequestBody Authentification authentification) {		
		 
		Authentification retourAuth = serviceAuthen.inscription(authentification);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(retourAuth.getId())
				.toUri();
	
		return ResponseEntity.created(location).body(retourAuth.getUtilisateur());
	}
		

//	@RequestMapping(value = "/inscription", method = RequestMethod.POST)
//	public ResponseEntity<Boolean> inscription(@RequestBody InscriptionWrapper inscriptionWrapper) {
//		
//		if ( !serviceAuthen.verifierEmail(inscriptionWrapper.getAuthentification().getEmail())) {
//			return ResponseEntity.noContent().build();
//		}
//		
//		inscriptionWrapper.getUtilisateur().setRoles(serviceRoles.findRole("user"));
//		Utilisateur retourU = serviceUser.inscriptionU( inscriptionWrapper.getUtilisateur());
//		inscriptionWrapper.getAuthentification().setUtilisateur(retourU);
//		Authentification retourAuth = serviceAuthen.inscription(inscriptionWrapper.getAuthentification());
//		
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(retourAuth.getId())
//				.toUri();
//		boolean verify = true;
//
//		return ResponseEntity.created(location).body(verify);
//	}
//		
	

//	@RequestMapping(value="/inscription", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)	
//	public ResponseEntity<Authentification> inscription (@RequestBody JSONObject jObjet) {	
//				
//		Authentification auth = new Authentification();
//		Utilisateur user = new Utilisateur();
//		
//		auth.setEmail((String)jObjet.get("email"));
//		auth.setMotPasse((String)jObjet.get("motPasse"));
//		
//		user.setPseudo((String)jObjet.get("pseudo"));
//		user.setTelephone((String)jObjet.get("telephone"));
//		
//		Utilisateur userRetour = serviceUtilisateur.inscription(user);
//		
//		auth.setUtilisateur(userRetour);
//		
//		Authentification authRetour = serviceAuthen.inscription(auth);	
//		
//		
//		if(authRetour == null || user == null) {
//			return ResponseEntity.noContent().build();
//		}		
//		URI location = ServletUriComponentsBuilder
//						.fromCurrentRequest()
//						.path("/{id}")
//						.buildAndExpand(authRetour.getId())
//						.toUri();		
//		
//		return ResponseEntity.created(location).header("toto","tata").body(authRetour);
//	}



} // fin accolade
