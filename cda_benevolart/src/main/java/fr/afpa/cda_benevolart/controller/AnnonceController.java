/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.afpa.cda_benevolart.bean.Annonce;
import fr.afpa.cda_benevolart.service.IServiceAnnonce;
import fr.afpa.cda_benevolart.service.IServiceUrl;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class AnnonceController {

	@Autowired
	IServiceAnnonce serviceAnnonce;

	@Autowired
	IServiceUrl serviceUrl;

	/**
	 * Methode permettant de chercher la liste d'annonce
	 * 
	 * @return	: MappingJacksonValue
	 */
	@RequestMapping(value = "/listAnnonce", method = RequestMethod.GET)
	public MappingJacksonValue listAnnonce() {

		List<Annonce> listAnnonce = serviceAnnonce.listAnnonce();
		MappingJacksonValue listAnnonceFinal = new MappingJacksonValue(listAnnonce);
		return listAnnonceFinal;

	}

	/**
	 * Methode permettant de sauvegarder une nouvelle annonce.
	 * 
	 * @param annonce : Une nouvelle annonce
	 * @return		  : ResponseEntity
	 */
	@RequestMapping(value = "/deposer", method = RequestMethod.POST)
	public ResponseEntity<Annonce> deposer(@RequestBody Annonce annonce) {

		Annonce retour = serviceAnnonce.deposer(annonce);

		if (retour == null) {
			return ResponseEntity.noContent().build();
		}

		URI location = ServletUriComponentsBuilder
													.fromCurrentRequest()
													.path("/{id}")
													.buildAndExpand(retour.getId())
													.toUri();

		return ResponseEntity.created(location).body(retour);
	}
	
	/**
	 * Methode permettant de chercher l'annonce concernee
	 * 
	 * @param id  : identifiant de l'annonce
	 * @return	: Annonce
	 */
	@RequestMapping(value="/getAnnonce/{id}", method=RequestMethod.GET)
	public MappingJacksonValue getAnnonce(@PathVariable Long id) {
		
		Annonce annonce = serviceAnnonce.getAnnonce(id);
		MappingJacksonValue retour = new MappingJacksonValue(annonce);
		return retour;
		
	}

	
	@RequestMapping(value="/recherche", method=RequestMethod.POST)
	public MappingJacksonValue recherche(@RequestBody Map<String, String> rechercheMap) {		
		String type = "";
		if( "demande".equals(rechercheMap.get("type")) ) {
			type="demande";
		} else {type="offre";}
		
		Long categorie=0L;
		if(rechercheMap.get("categorie") != null) {
			categorie = Long.parseLong(rechercheMap.get("categorie"));
		} 
		
		String cle="";
		if(rechercheMap.get("cle") != null) {
			cle = "%"+rechercheMap.get("cle")+"%";
		}
		
		String ville=null;
		if(rechercheMap.get("ville")!=null) {
			ville=rechercheMap.get("ville");
		}
		
		LocalDateTime debut=  null;
		//String debut=null;
		if(rechercheMap.get("debut") != null) {
			debut= LocalDateTime.parse(rechercheMap.get("debut"));
			//debut = rechercheMap.get("debut");
		}
		
		LocalDateTime fin=  null;
		//String fin=null;
		if(rechercheMap.get("fin")!=null) {
			fin=LocalDateTime.parse(rechercheMap.get("fin"));
			//fin=rechercheMap.get("fin");
		}		
		
		
		List<Annonce> annonces = serviceAnnonce.recherche(type, categorie, cle, ville);
		MappingJacksonValue retour = new MappingJacksonValue(annonces);
		return retour;
		
	}
	
	@RequestMapping(value="/mesAnnonces/{id}", method=RequestMethod.GET)
	public MappingJacksonValue mesAnnonces(@PathVariable Long id) {

		List<Annonce> listAnnonce = serviceAnnonce.mesAnnonces(id);
		MappingJacksonValue listAnnonceFinal = new MappingJacksonValue(listAnnonce);
		return listAnnonceFinal;

	}
	
	
	
} // fin accolade
