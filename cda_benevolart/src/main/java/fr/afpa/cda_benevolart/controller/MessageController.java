/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.afpa.cda_benevolart.bean.Message;
import fr.afpa.cda_benevolart.service.IServiceMessage;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class MessageController {
	
	@Autowired
	IServiceMessage serviceMessage; 
	
	/**
	 * Methode permettant de sauvegarder une nouvelle message.
	 * @param message	: Une nouvelle message
	 * @return			: : ResponseEntity
	 */
	@RequestMapping(value="/saveMsg", method = RequestMethod.POST)
	public ResponseEntity<Message> saveMsg (@RequestBody Message message) {
		Message retour =  serviceMessage.save(message);
		
		if (retour == null) {
			return ResponseEntity.noContent().build();
		} 
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{/id}").buildAndExpand(retour.getId()).toUri();
		
		return ResponseEntity.created(location).body(retour);
	}
	
	@RequestMapping(value="/allMsg", method=RequestMethod.GET)
	public MappingJacksonValue allMsg () {
		List<Message> list = serviceMessage.ltMessage();
		MappingJacksonValue listAnnonceFinal = new MappingJacksonValue(list);
		return listAnnonceFinal;
	}
	
	

}
