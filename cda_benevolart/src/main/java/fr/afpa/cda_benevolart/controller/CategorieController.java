/**
 * 
 */
package fr.afpa.cda_benevolart.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.cda_benevolart.bean.Categorie;
import fr.afpa.cda_benevolart.service.IServiceCategorie;

/**
 * @author Eun Kyung HAN
 *
 */
@RestController
@CrossOrigin("http://localhost:4200")
public class CategorieController {
	
	@Autowired
	IServiceCategorie serviceCategorie;
	
	
	/**
	 * Methode permettant de chercher la liste de categorie
	 * @return : MappingJacksonValue
	 */
	@RequestMapping(value="/listeCategorie", method=RequestMethod.GET)
	public MappingJacksonValue listeCategorie() {
		List<Categorie> retour = serviceCategorie.listeCategorie();
		MappingJacksonValue liste = new MappingJacksonValue(retour);
		return liste;
	}

}
