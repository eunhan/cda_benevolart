/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import fr.afpa.cda_benevolart.bean.Annonce;

/**
 * @author Eun Kyung HAN
 *
 */

public interface IServiceAnnonce {

	/**
	 * Service afin de chercher une liste de toutes les annonces
	 * 
	 * @return : Une liste d'annonce
	 */
	public List<Annonce> listAnnonce();

	/**
	 * Service afin de sauvegarder l'annonce
	 * 
	 * @param annonce : Une nouvelle annonce
	 * @return : Annonce
	 */
	public Annonce deposer(Annonce annonce);

	/**
	 * Service afin de recuperer l'annonce concernee
	 * 
	 * @param id : identifiant de l'annonce
	 * @return : Annonce
	 */
	public Annonce getAnnonce(Long id);

	/**
	 * Service afin de rechercher avec les criteres
	 * 
	 * @param type : Type soit demande soit offre
	 * @param categorie	: categorie
	 * @param cle : mot cle
	 * @param ville : ville
	 * @param debut : Date et heure debut
	 * @param fin : Date et heure fin
	 * @return : Une liste d'annonce
	 */
	public List<Annonce> recherche(String type, Long categorie, String cle, String ville);
	
	
	public List<Annonce> mesAnnonces(Long id);
}
