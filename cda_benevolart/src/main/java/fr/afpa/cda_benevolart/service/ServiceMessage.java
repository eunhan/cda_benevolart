/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Message;
import fr.afpa.cda_benevolart.dao.IDaoMessage;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceMessage implements IServiceMessage {

	@Autowired
	IDaoMessage daoMessage;

	@Override
	public Message save(Message message) {
		return daoMessage.save(message);
	}

	@Override
	public List<Message> ltMessage() {		
		return daoMessage.findAll();
	}

}
