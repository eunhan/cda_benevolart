/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import fr.afpa.cda_benevolart.bean.Url;

/**
 * @author Eun Kyung HAN
 *
 */
public interface IServiceUrl {

	/**
	 * Service permettant de sauvegarder un nouveau url
	 * 
	 * @param listeUrl : Une liste d'url de l'utilisateur
	 * @return : Une liste d'url
	 */
	public List<Url> ajouter(List<Url> listeUrl);
}
