/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Annonce;
import fr.afpa.cda_benevolart.dao.IDaoAnnonce;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceAnnonce implements IServiceAnnonce {

	@Autowired
	IDaoAnnonce daoAnnonce;
	

	@Override
	public List<Annonce> listAnnonce() {

		return daoAnnonce.findByVisibilite(true);
	}

	@Override
	public Annonce deposer(Annonce annonce) {
		return daoAnnonce.save(annonce);
	}

	@Override
	public Annonce getAnnonce(Long id) {		
		return daoAnnonce.findById(id).get();
	}

//	@Override
//	public List<Annonce> recherche(Long categorie, String cle, String ville) {
//		List<Annonce> annonces = daoAnnonce.findByCritere( categorie, cle);
//		
//		for(int i = 0; i<annonces.size(); i++) {
//			if( !annonces.get(i).getAdresse().getVille().contains(ville)) {
//				annonces.remove(i);
//			}
//		}
//				
//		return annonces;
//	}

	@Override
	public List<Annonce> recherche(String type, Long categorie, String cle, String ville) {
		List<Annonce> annonces = new ArrayList<Annonce>();
		List<Annonce> retour=new ArrayList<Annonce>();
		if("demande".equals(type)) {
			 annonces = daoAnnonce.findByCritereDemande(categorie, cle);
		} else {
			annonces=daoAnnonce.findByCritereOffre(categorie, cle);
		}
				
		for(int i = 0; i<annonces.size(); i++) {
			if( annonces.get(i).getAdresse().getVille().contains(ville)) {
				retour.add(annonces.get(i));
			}
		}				
		return retour;
	}

	@Override
	public List<Annonce> mesAnnonces(Long id) {		
		return daoAnnonce.findByUtilisateurId(id);
	}
	
	
}
