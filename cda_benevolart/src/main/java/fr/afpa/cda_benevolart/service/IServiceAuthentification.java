/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import fr.afpa.cda_benevolart.bean.Authentification;
import fr.afpa.cda_benevolart.bean.Utilisateur;

/**
 * @author Eun Kyung HAN
 *
 */

public interface IServiceAuthentification {

	/**
	 * Service afin de verifier l'email et le mot de passe saisie par l'utilisateur
	 * 
	 * @param authentification : les donnees saisies par l'utilisateur
	 * @return : l'utilisateur
	 */
	public Utilisateur login(Authentification authentification);
	
	
	//public Utilisateur login2(Authentification authentification);

	/**
	 * Service afin de creer un nouveau authentification
	 * 
	 * @param authentification		: les donnees saisies par l'utilisateur
	 * @return : L'authentification
	 */
	public Authentification inscription(Authentification authentification);

	/**
	 * Service permettan de verifier si l'email est deja exist.
	 * 
	 * @param email : email saisi par l'utilisateur
	 * @return : s'il n'est pas exist return true sinon false
	 */
	public boolean verifierEmail(String email);

	// test Postman
	public List<Authentification> listAuth();
	
	
	public Authentification getAuthentification(Long id);

} // fin accolade
