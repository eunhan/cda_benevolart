/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import fr.afpa.cda_benevolart.bean.Categorie;

/**
 * @author Eun Kyung HAN
 *
 */

public interface IServiceCategorie {
	
	/**
	 * Service afin de chercher une liste de toutes les Categorie
	 * 
	 * @return	: Une liste de categorie
	 */
	public List<Categorie> listeCategorie();

}
