/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Categorie;
import fr.afpa.cda_benevolart.dao.IDaoCategorie;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceCategorie implements IServiceCategorie{
	
	 @Autowired
	 IDaoCategorie daoCategorie;

	@Override
	public List<Categorie> listeCategorie() {		
		return daoCategorie.findAll();
	}

}
