/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import fr.afpa.cda_benevolart.bean.Message;

/**
 * @author Eun Kyung HAN
 *
 */
public interface IServiceMessage {
	
	public Message save(Message message);

	public List<Message> ltMessage();
}
