/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import fr.afpa.cda_benevolart.bean.Utilisateur;

/**
 * @author Eun Kyung HAN
 *
 */
public interface IServiceUtilisateur {

	/**
	 * Service afin de creer un nouveau utilisateur
	 * 
	 * @param authentification : les donnees saisies par l'utilisateur
	 * @return : l'utilisateur
	 */
	Utilisateur inscriptionU(Utilisateur utilisateur);

} // fin accolade
