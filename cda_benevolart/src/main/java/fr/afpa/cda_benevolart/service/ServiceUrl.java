/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Url;
import fr.afpa.cda_benevolart.dao.IDaoUrl;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceUrl implements IServiceUrl{
	
	@Autowired
	IDaoUrl daoUrl;
	
	public List<Url> ajouter(List<Url>  listeUrl) {
		return daoUrl.saveAll(listeUrl);	
	}

}
