/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Authentification;
import fr.afpa.cda_benevolart.bean.Utilisateur;
import fr.afpa.cda_benevolart.dao.IDaoAuthentification;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceAuthentification implements IServiceAuthentification {

	@Autowired
	IDaoAuthentification daoAuthen;

	@Override
	public Utilisateur login(Authentification authentification) {
		
		Authentification retour = daoAuthen.findByEmailAndMotPasse(authentification.getEmail(), authentification.getMotPasse());
		
		if (retour != null) {
			return retour.getUtilisateur();
		}
		
		return null;
	}

/*	@Override
	public Utilisateur login2(Authentification authentification) {
		
		Authentification retour = daoAuthen.findByEmailAndMotPasseAndActive(authentification.getEmail(), authentification.getMotPasse());
		
		if (retour != null) {
			return retour.getUtilisateur();
		}
		
		return null;
	}*/
	
	@Override
	public Authentification inscription(Authentification authentification) {
	
		return daoAuthen.save(authentification);		
	}

	@Override
	public boolean verifierEmail(String email) {

		if (daoAuthen.findByEmail(email) == null) {
			return true;
		}
		return false;
	}

	// Test postman
	@Override
	public List<Authentification> listAuth() {

		return daoAuthen.findAll();
	}

	@Override
	public Authentification getAuthentification(Long id) {
		
		return daoAuthen.findByUtilisateurId(id);
	}
	
	

}// fin accolade
