/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Roles;
import fr.afpa.cda_benevolart.dao.IDaoRoles;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceRoles implements IServiceRoles {
	
	@Autowired
	IDaoRoles daoRoles;
	
	@Override
	public Roles findRole(Long id) {
		return daoRoles.findById(id).get();
	}
	
	
	
	@Override
	public Roles findRole(String poste) {		
		return daoRoles.findByPoste(poste);
	}



	
	

}
