/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import fr.afpa.cda_benevolart.bean.Roles;

/**
 * @author Eun Kyung HAN
 *
 */
public interface IServiceRoles {
	
	public Roles findRole(Long id);
	
	public Roles findRole(String poste);

}
