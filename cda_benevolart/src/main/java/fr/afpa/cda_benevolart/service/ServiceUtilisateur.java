/**
 * 
 */
package fr.afpa.cda_benevolart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.cda_benevolart.bean.Utilisateur;
import fr.afpa.cda_benevolart.dao.IDaoUtilisateur;

/**
 * @author Eun Kyung HAN
 *
 */
@Service
public class ServiceUtilisateur implements IServiceUtilisateur{
	
	@Autowired
	IDaoUtilisateur daoUtilisateur;

	@Override
	public Utilisateur inscriptionU(Utilisateur utilisateur) {		
		return daoUtilisateur.save(utilisateur);
	}

	

} // fin accolade
