package fr.afpa.cda_benevolart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdaBenevolartApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdaBenevolartApplication.class, args);
	}

}
