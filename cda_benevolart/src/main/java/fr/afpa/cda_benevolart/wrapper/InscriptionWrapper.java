/**
 * 
 */
package fr.afpa.cda_benevolart.wrapper;

import fr.afpa.cda_benevolart.bean.Authentification;
import fr.afpa.cda_benevolart.bean.Utilisateur;
import lombok.Data;

/**
 * @author Eun Kyung HAN
 *
 */

@Data
public class InscriptionWrapper {
	
	Authentification authentification;
	Utilisateur utilisateur;

}
